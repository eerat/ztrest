package SimpleModule

import "fmt"

func ShowAllItems(items []string) {
	for key, value := range items {
		fmt.Printf(" -->  %v %v \n", key, value)
	}
}