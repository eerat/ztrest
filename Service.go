package main

import (
	"database/sql"
	"github.com/coopernurse/gorp"
	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"strconv"
	"time"
	"fmt"
)

var dbmap = initDb()

func main() {

	defer dbmap.Db.Close()

	router := gin.Default()
	router.Use(Cors());
	router.GET("/articles", ArticlesList)
	router.POST("/articles", ArticlePost)
	router.GET("/articles_all", ArticlesListWithDataKey)
	router.GET("/articles/:id", ArticlesDetail)
	router.DELETE("/articles/:id", ArticleDelete)
	router.OPTIONS("/articles", OptionsArticle)      // POST
	router.OPTIONS("/articles/:id", OptionsArticle)  // PUT, DELETE
	fmt.Println("Running...");
	router.Run(":8010")
}

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST,GET,DELETE,POST,PUT,UPDATE,OPTIONS")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Next()
	}
}

func OptionsArticle(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Methods", "DELETE,POST,PUT")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	c.Next()
}

type Article struct {
	Id      int64 `db:"article_id" json:"id"`
	Created int64 `json:"created"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

func createArticle(title, body string) Article {
	article := Article{
		Created: time.Now().UnixNano(),
		Title:   title,
		Content: body,
	}

	err := dbmap.Insert(&article)
	checkErr(err, "Insert failed")
	return article
}

func deleteArticle(idx int64) int64 {
	article := Article{
		Id:idx,
	}
	res, err := dbmap.Delete(&article)
	checkErr(err, "Delete failed")
	return res
}

func getArticle(article_id int) Article {
	article := Article{}
	err := dbmap.SelectOne(&article, "select * from articles where article_id=?", article_id)
	checkErr(err, "SelectOne failed")
	return article
}

func ArticlesList(c *gin.Context) {
	var articles []Article
	_, err := dbmap.Select(&articles, "select * from articles order by article_id")
	checkErr(err, "Select failed")
	content := gin.H{}
	for k, v := range articles {
		content[strconv.Itoa(k)] = v
	}
	c.JSON(200, content)
}

func ArticlesListWithDataKey(c *gin.Context) {
	var articles []Article
	_, err := dbmap.Select(&articles, "select * from articles order by article_id")
	checkErr(err, "Select failed")
	content := gin.H{}
	content["data"] = articles
	c.JSON(200, content)
}

func ArticlesDetail(c *gin.Context) {
	article_id := c.Params.ByName("id")
	a_id, _ := strconv.Atoi(article_id)
	article := getArticle(a_id)
	content := gin.H{"title": article.Title, "content": article.Content}
	c.JSON(200, content)
}

func ArticleDelete(c *gin.Context) {
	article_id := c.Params.ByName("id")
	a_id, _ := strconv.Atoi(article_id)

	response := deleteArticle(int64(a_id))
	content := gin.H{"data": response}
	c.JSON(200, content)
}

func ArticlePost(c *gin.Context) {
	var json Article

	c.Bind(&json) // This will infer what binder to use depending on the content-type header.
	article := createArticle(json.Title, json.Content)
	if article.Title == json.Title {
		content := gin.H{"data": article.Id}
		c.JSON(201, content)
	} else {
		c.JSON(500, gin.H{"result": "An error occured"})
	}
}

func initDb() *gorp.DbMap {
	db, err := sql.Open("sqlite3", "db.sqlite3")
	checkErr(err, "sql.Open failed")

	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}

	dbmap.AddTableWithName(Article{}, "articles").SetKeys(true, "Id")

	err = dbmap.CreateTablesIfNotExists()
	checkErr(err, "Create tables failed")

	return dbmap
}

func checkErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}

